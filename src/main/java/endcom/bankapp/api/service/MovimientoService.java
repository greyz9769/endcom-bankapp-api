package endcom.bankapp.api.service;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import endcom.bankapp.api.domain.Cuenta;
import endcom.bankapp.api.domain.Movimiento;
import endcom.bankapp.api.dto.CodigoMovimientoDTO;
import endcom.bankapp.api.dto.MovimientoAccDTO;
import endcom.bankapp.api.dto.MovimientoDTO;
import endcom.bankapp.api.dto.MovimientosCuentaDTO;
import endcom.bankapp.api.dto.ResponseDTO;
import endcom.bankapp.api.repository.CuentaRepository;
import endcom.bankapp.api.repository.MovimientoRepository;
import endcom.bankapp.api.utils.DisableSSL;

@Service
public class MovimientoService {
	
	@Autowired
	MovimientoRepository movRepository;

	@Autowired
	CuentaRepository cuentaRepository;

	@Value("${url.api}")
	private String apiAccount;
	
	@Autowired
	MongoTemplate mongot;
	
	public ResponseDTO crearMovimiento(@Valid MovimientoDTO movimiento) {
		// TODO Auto-generated method stub
		Optional<Cuenta> cuenta = cuentaRepository.findByAccount(movimiento.getAccount());
		if(cuenta.isPresent()) {
			float balance= cuenta.get().getBalance()-movimiento.getAmount();
			if(balance>0) {
				Movimiento mov = new Movimiento();
				mov.setCreateDate(new Date());
				mov.setDescription(movimiento.getDescription());
				mov.setAmount(movimiento.getAmount());
				mov.setAccount(cuenta.get().getAccount());
				cuenta.get().setBalance(balance);
				cuentaRepository.save(cuenta.get());
				movRepository.save(mov);
				
				ResponseDTO resp = new ResponseDTO(HttpStatus.OK.value(), "Movimiento registrado");
				return resp;
				
			}else {
				ResponseDTO resp = new ResponseDTO(HttpStatus.BAD_REQUEST.value(), "No cuentas con saldo suficiente");
				return resp;
			}
		}
		ResponseDTO resp = new ResponseDTO(HttpStatus.BAD_REQUEST.value(), "El numero de cuenta no existe");
		return resp;
	}

	public ResponseDTO movimientoAccountUsuario(@Valid MovimientoAccDTO movAccount) {
		// TODO Auto-generated method stub
		String apiURL = apiAccount + "/"+"movements-type";
		CodigoMovimientoDTO[] catMovimientos = getDataFromApi(apiURL);
		for(CodigoMovimientoDTO movimientoCode:catMovimientos) {
			if(movimientoCode.getCode().equals(movAccount.getMovementCode())) {
				Optional<Movimiento> movimiento = movRepository.findById(movAccount.getId());
				if(movimiento.isPresent()) {
					movimiento.get().setMovementCode(movAccount.getMovementCode());
					movRepository.save(movimiento.get());
					
					ResponseDTO resp = new ResponseDTO(HttpStatus.OK.value(), "Movimiento actualizado");
					return resp;
				}else {
					ResponseDTO resp = new ResponseDTO(HttpStatus.BAD_REQUEST.value(), "El id del movimiento no existe");
					return resp;
				}
				
			}
		}
		ResponseDTO resp = new ResponseDTO(HttpStatus.BAD_REQUEST.value(), "El tipo de movimiento no existe");
		return resp;
	}
	
	public CodigoMovimientoDTO[] getDataFromApi(String apiURL) {
		DisableSSL ssl = new DisableSSL();
		RestTemplate restTemplate;
		try {
			restTemplate = ssl.DisableSSL();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			ResponseEntity<CodigoMovimientoDTO[]> responseEntity = restTemplate.getForEntity(apiURL, CodigoMovimientoDTO[].class, 
					HttpMethod.GET);
			
			CodigoMovimientoDTO[] listCodMov = responseEntity.getBody();
			
			return listCodMov;
			
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public MovimientosCuentaDTO consultarMovimientosPorCuenta(String account) {
		// TODO Auto-generated method stub
		Query query = new Query();
		query.addCriteria(Criteria.where("account").is(account));
		List<Movimiento> movByAccount= mongot.find(query, Movimiento.class);
		
		MovimientosCuentaDTO response = new MovimientosCuentaDTO(HttpStatus.OK.value(), "Movimientos de cuenta");
		response.setAccount(account);
		response.setMovements(movByAccount);
		
		return response;
	}

}
