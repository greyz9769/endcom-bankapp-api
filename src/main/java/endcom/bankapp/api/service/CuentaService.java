package endcom.bankapp.api.service;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import endcom.bankapp.api.domain.Cuenta;
import endcom.bankapp.api.dto.CuentaDTO;
import endcom.bankapp.api.dto.SaldoDTO;
import endcom.bankapp.api.dto.UsuarioDTO;
import endcom.bankapp.api.repository.CuentaRepository;
import endcom.bankapp.api.utils.DisableSSL;
import endcom.bankapp.api.utils.TextUtils;

@Service
public class CuentaService {

	@Autowired
	CuentaRepository cuentaRepository;

	TextUtils txtUtils;

	@Value("${url.api}")
	private String apiAccount;

	public Cuenta getDataFromApi(String apiURL, UsuarioDTO user) {
		Gson gson = new Gson();
		String jsonBody = gson.toJson(user);
		DisableSSL ssl = new DisableSSL();
		RestTemplate restTemplate;
		try {
			restTemplate = ssl.DisableSSL();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> httpEntity = new HttpEntity<>(jsonBody, headers);
			ResponseEntity<Cuenta> responseEntity = restTemplate.postForEntity(apiURL, httpEntity, Cuenta.class);
			return responseEntity.getBody();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public CuentaDTO crearCuenta(UsuarioDTO user) {
		// Se crea la URL del Api
		String apiURL = apiAccount + "/"+"account";

//		//Se obtienen los datos del Api
		Cuenta cuenta = getDataFromApi(apiURL, user);

//		//Creamos el número de cuenta
		txtUtils = new TextUtils();
		String id = txtUtils.getDecimalFormat(Integer.parseInt(cuenta.getId()));
		String accountFormat = cuenta.getAccount() + id;
//		
//		//Seteamos los datos del DTO al Entity para crear la cuenta en la BD
		cuenta.setId(null);
		cuenta.setAccount(accountFormat);
		cuentaRepository.save(cuenta);
//		
//		//Creamos el response que se devuelve al usuario
		CuentaDTO cuentaDto = new CuentaDTO(HttpStatus.OK.value(), "Cuenta creada");
		cuentaDto.setId(cuenta.getId());
		cuentaDto.setAccount(accountFormat);
		cuentaDto.setBalance(cuenta.getBalance());
		cuentaDto.setName(cuenta.getName());
		cuentaDto.setMail(cuenta.getMail());
//	
		return cuentaDto;

	}

	public SaldoDTO agregarSaldo(SaldoDTO saldo) {
		// TODO Auto-generated method stub
		Optional<Cuenta> cuenta = cuentaRepository.findByAccount(saldo.getAccount());
		if (cuenta.isPresent()) {
			cuenta.get().setBalance(cuenta.get().getBalance() + saldo.getBalance());
			cuentaRepository.save(cuenta.get());

			saldo.setStatusCode(HttpStatus.OK.value());
			saldo.setMessage("Saldo actualizado");
			saldo.setBalance(cuenta.get().getBalance());
			return saldo;
		}
		saldo.setStatusCode(HttpStatus.NOT_FOUND.value());
		saldo.setMessage("El número de cuenta no existe en la Base de Datos");
		return saldo;
	}

	public CuentaDTO consultarCuenta(String account) {
		Optional<Cuenta> cuenta = cuentaRepository.findByAccount(account);
		if (cuenta.isPresent()) {
			CuentaDTO cuentaDto = new CuentaDTO(HttpStatus.OK.value(),"Información de cuenta");
			cuentaDto.setId(cuenta.get().getId());
			cuentaDto.setAccount(account);
			cuentaDto.setBalance(cuenta.get().getBalance());
			cuentaDto.setName(cuenta.get().getName());
			cuentaDto.setMail(cuenta.get().getMail());
			return cuentaDto;
		}
		CuentaDTO cuentaDto = new CuentaDTO(HttpStatus.NOT_FOUND.value(),"La cuenta no existe en la Base de Datos");
		return cuentaDto;
	}
}
