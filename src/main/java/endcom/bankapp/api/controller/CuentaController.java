package endcom.bankapp.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import endcom.bankapp.api.dto.CuentaDTO;
import endcom.bankapp.api.dto.SaldoDTO;
import endcom.bankapp.api.dto.UsuarioDTO;
import endcom.bankapp.api.service.CuentaService;


@Controller
public class CuentaController {

	@Autowired
	private CuentaService cuentaServ;
	
	@CrossOrigin()
	@RequestMapping(value = "/create-account", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody CuentaDTO createAccount(@Valid @RequestBody UsuarioDTO user) {
		return cuentaServ.crearCuenta(user);
	}
	
	@CrossOrigin()
	@RequestMapping(value = "/account", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody SaldoDTO agregarSaldo(@Valid @RequestBody SaldoDTO saldo) {
		return cuentaServ.agregarSaldo(saldo);
	}
	
	@CrossOrigin()
	@RequestMapping(value = "/account/{account}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody CuentaDTO consultarCuenta(@PathVariable String account) {
		return cuentaServ.consultarCuenta(account);
	}

}
