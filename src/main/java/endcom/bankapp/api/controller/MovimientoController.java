package endcom.bankapp.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import endcom.bankapp.api.dto.MovimientoAccDTO;
import endcom.bankapp.api.dto.MovimientoDTO;
import endcom.bankapp.api.dto.MovimientosCuentaDTO;
import endcom.bankapp.api.dto.ResponseDTO;
import endcom.bankapp.api.service.MovimientoService;

@Controller
@RequestMapping(path = "/account")
public class MovimientoController {

	@Autowired
	private MovimientoService movimientoServ;
	
	@CrossOrigin()
	@RequestMapping(value = "/movement", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseDTO crearMovimiento(@Valid @RequestBody MovimientoDTO movimiento) {
		return movimientoServ.crearMovimiento(movimiento);
	}
	
	@CrossOrigin()
	@RequestMapping(value = "/movement", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseDTO movimientoAccountUsuario(@Valid @RequestBody MovimientoAccDTO movAccount) {
		return movimientoServ.movimientoAccountUsuario(movAccount);
	}
	
	@CrossOrigin()
	@RequestMapping(value = "/movement/{account}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody MovimientosCuentaDTO consultarMovimientosPorCuenta(@PathVariable String account) {
		return movimientoServ.consultarMovimientosPorCuenta(account);
	}
}	
