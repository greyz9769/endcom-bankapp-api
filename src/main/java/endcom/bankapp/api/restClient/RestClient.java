package endcom.bankapp.api.restClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RestClient {

	public JsonObject getJSONDataByGet(String urlTo) {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
	            .url(urlTo)
	            .build();

	        Response response=null;
			try {
				response = client.newCall(request).execute();
				Gson g = new Gson();
				JsonObject convertedObject = new Gson().fromJson(response.body().string(), JsonObject.class);
				return convertedObject;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return null;
	}
	
	public String callToServicePost(String bodyParams, String url){

		HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url+"/");

        post.setEntity(new StringEntity(bodyParams, Charset.forName("UTF-8")));
        
        try {
            post.addHeader("Content-Type", "application/json");
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity);
            return content;
            
        } catch (IOException e) {
            e.printStackTrace();
        }
		return null;
	}
}
