package endcom.bankapp.api.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class SaldoDTO extends ResponseDTO {

	@NotEmpty
	@Size(min = 11, max= 11, message = "El número de cuenta debe contener 11 caracteres")
	String account;
	
	float balance;

	public SaldoDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

}
