package endcom.bankapp.api.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class MovimientoAccDTO {
	@NotEmpty
	String id;
	
	@NotEmpty
	@Size(min = 4, max = 4, message ="El campo movementCode  debe contener 4 caracteres")
	String movementCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMovementCode() {
		return movementCode;
	}
	public void setMovementCode(String movementCode) {
		this.movementCode = movementCode;
	}
	
	
}
