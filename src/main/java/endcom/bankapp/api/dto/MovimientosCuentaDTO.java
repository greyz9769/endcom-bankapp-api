package endcom.bankapp.api.dto;

import java.util.List;

import endcom.bankapp.api.domain.Movimiento;

public class MovimientosCuentaDTO extends ResponseDTO {

	String account;
	List<Movimiento> movements;

	public MovimientosCuentaDTO(int statusCode, String message) {
		super(statusCode, message);
		// TODO Auto-generated constructor stub
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public List<Movimiento> getMovements() {
		return movements;
	}

	public void setMovements(List<Movimiento> movements) {
		this.movements = movements;
	}

}
