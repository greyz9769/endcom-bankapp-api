package endcom.bankapp.api.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class MovimientoDTO {
	@NotEmpty
	@Size(min = 11, max = 11, message = "El campo account, debe contener 11 caracteres")
	String account;

	@Min(value = 1, message = "El campo amount no debe ser 0")
	float amount;

	String description = "";

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
