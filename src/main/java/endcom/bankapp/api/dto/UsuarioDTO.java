package endcom.bankapp.api.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UsuarioDTO {
	@NotEmpty
	@Size(min = 3, message ="El campo name, debe contener al menos 3 caracteres")
	String name;
	
	@Email(message ="El campo mail, no es una dirección de correo válida")
	String mail;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	 
	 

}
