package endcom.bankapp.api;

import javax.net.ssl.SSLContext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class ApiApplication extends SpringBootServletInitializer  {

		public static void main(String[] args) {
			try {
				SSLContext ctx = SSLContext.getInstance("TLSv1.2");
				ctx.init(null, null, null);
				SSLContext.setDefault(ctx);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			SpringApplication.run(ApiApplication.class, args);
		}

}
