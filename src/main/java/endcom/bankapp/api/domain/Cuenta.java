package endcom.bankapp.api.domain;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bankapp_account")
public class Cuenta implements Serializable {

	@Id
	String id;
	
	@NotEmpty
	@Size(min = 3, message ="El campo nombre debe contener al menos 3 caracteres")
	String name;
	
	@NotEmpty
	@Email(message ="El campo mail no es una dirección de correo válida")
	String mail;
	
	@NotEmpty
	@Size(min = 11, max= 11, message ="El número de cuenta debe contener 11 caracteres")
	String account;
	
	@NotEmpty
	float balance = 1000f;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

}
