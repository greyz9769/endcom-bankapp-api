package endcom.bankapp.api.domain;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bankapp_movement")
public class Movimiento implements Serializable {
	@Id
	String id;
	
	@NotEmpty
	@Size(min = 4, max = 4, message ="El movementCode campo  debe contener 4 caracteres")
	String movementCode = "M001";
	
	String description;
	
	@Min(value = 1, message = "El campo amount no debe ser 0")
	float amount;
	
	@NotEmpty
	@Size(min = 11, max = 11, message ="El campo account debe contener 11 caracteres")
	String account;
	
	@NotEmpty
	Date createDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMovementCode() {
		return movementCode;
	}

	public void setMovementCode(String movementCode) {
		this.movementCode = movementCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
		
}
