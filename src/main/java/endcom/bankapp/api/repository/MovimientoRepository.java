package endcom.bankapp.api.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import endcom.bankapp.api.domain.Movimiento;

public interface MovimientoRepository extends MongoRepository<Movimiento, String> {

}
