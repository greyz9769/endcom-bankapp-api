package endcom.bankapp.api.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import endcom.bankapp.api.domain.Cuenta;

public interface CuentaRepository extends MongoRepository<Cuenta,String> {

	 Optional<Cuenta> findByAccount(String account);
	

}
