package endcom.bankapp.api.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TextUtils {
	
	public String getDecimalFormat(int id) {
		NumberFormat formatter = new DecimalFormat("000");
		return formatter.format(id);
	}

}
