#### Instalación de MongoDB:
1. Abrir el sitio: https://www.mongodb.com/try#community 
1. Seleccionar la descarga para un ambiente Windows, descargar e instalar en su versión completa
1. Una vez instalada, abrimos la aplicación de MongoDB Compass y nos permite tener acceso a un servidor local
1. Abrir en compass la terminal de mongo y escribir los siguientes comandos
    1. **use bankapp**
    1. **db.createUser({user: "graciela",pwd: "endcombankappapi",roles: [ { role: "readWrite", db: "bankapp" } ]})**

#### Pasos para levantar el proyecto
1. Abrir **Spring tool suite/Eclipse/Netbeans/etc.** e importar el Proyecto como un proyecto Maven
2. Una vez abierto por algú IDE de desarrollo se deberá configurar lo siguiente:
	1. Build path: SDK Java 1.8
	1. Run configuration:
		1. Goals: clean install
		1. Skip test
3.	Levantar como una aplicación Spring Boot App

#### Pasos para desplegar el proyecto (Opcional):
1. Abrir la carpeta del proyecto
2. Compilar con el comando  ./mvnw package
3. Se generará una carpeta de nombre target donde se encontrará el archivo **api-0.0.1-SNAPSHOT.war**, este puede ser desplegado en cualquier servidor de aplicaciones.

#### Instrucciones para consumir el API
1. Si se quiere usar el API desde la aplicación de POSTMAN, se deberá importar el archivo de Documentación que se encuentra dentro de la carpeta **postman_api**, para acceder da clic en el siguiente enlace: [Endcom Bankapp.postman_collection](https://gitlab.com/greyz9769/endcom-bankapp-api/postman_api) 
1. Si se quiere usar y conocer los detalles de la documentación del API se puede hacer directamente desde el vínculo donde se publicó por medio del gestor de POSTMAN, para verlo da clic en el siguiente enlace: [API Bankapp](https://documenter.getpostman.com/view/12283459/UVXkoaeE#76e83ed7-54b8-46c3-bbfa-7df135a26530)


